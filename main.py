from scrapy import cmdline

# https://doc.scrapy.org/en/latest/intro/tutorial.html
# https://doc.scrapy.org/en/latest/topics/downloader-middleware.html?highlight=cache

if __name__ == '__main__':
    cmdline.execute("scrapy crawl nep".split())
