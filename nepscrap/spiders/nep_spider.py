from typing import Tuple

import scrapy
from scrapy.http import TextResponse
from bs4 import BeautifulSoup

# a very helpful XPath tip: when using an xpath subselector (on another selector), "//" still searches for the whole
# document - use ".//" to search from the currently selected element
# https://doc.scrapy.org/en/latest/topics/selectors.html#working-with-relative-xpaths


COORDINATES_STATISTICAL_AREA = {
    "gorenjska": (46.2976, 14.377),
    "dolenjska": (45.7081, 14.9606),
    "posavska": (45.909, 15.581),
    "zasavska": (46.1304, 15.0001),
    "savinjska": (46.2522, 15.1637),
    "koroška": (46.6127, 15.7077),
    "lj-mesto": (46.0508, 14.5074),
    "podravska": (46.4918, 16.0871),
    "lj-okolica": (45.9688, 14.3028),
    "notranjska": (45.7225, 14.3646),
    "j. primorska": (45.5222, 13.6141),
    # sic
    "s.primorska": (45.859, 13.722)
}


class NepSpider(scrapy.Spider):
    name = "nep"
    sitemap = "https://www.nepremicnine.net/sitemap.xml"

    def start_requests(self):
        yield scrapy.Request(url=self.sitemap, callback=self.parse_sitemap_to_base)

    def parse(self, response):
        # we don't use this and start_requests doesn't use this
        pass

    def parse_sitemap_to_base(self, response: TextResponse):
        allowed_listing_types = {"prodaja", "oddaja"}
        excluded_areas = {"hrvaska", "ostale-drzave", "primorsko-goranska", "istrska", "zagreb-city", "zagrebska",
                          "dubrovniska-neretvanska", "splitsko-dalmatinska", "sibenisko-kninska", "zadarska",
                          "osijesko-baranjska", "vukovarsko-sremska", "virovitisko-podravska", "pozesko-slavonska",
                          "brodsko-posavska", "medzimurska", "varazdinska", "bjelovarsko-bilogorska",
                          "sisasko-moslavinska", "karlovska", "koprivnisko-krizevska", "krapinsko-zagorska",
                          "lisko-senjska", "slovenija"}
        allowed_property_types = {"stanovanje", "hisa", "posest"}

        selector = response.xpath("//*[local-name() = 'loc']/text()")
        url_elements = selector.extract()
        for el in url_elements:
            subpart = el.split("https://www.nepremicnine.net/")[1].strip("/")
            triplet = subpart.split("/")

            if len(triplet) != 3:
                continue

            triplet[0] = triplet[0].split("oglasi-")[1]
            if triplet[0] not in allowed_listing_types or triplet[1] in excluded_areas or triplet[
                2] not in allowed_property_types:
                continue

            # yield {
            #     "element": el,
            #     "subpart": subpart,
            #     "triplet": triplet
            # }
            yield scrapy.Request(url=el, callback=self.parse_base_add_administrative_unit)

    def parse_base_add_administrative_unit(self, response: TextResponse):
        # don't select lists without items (without a div that has an item count)
        administrative_unit_selector = response.xpath("//ul[@id = 'facetUE']/li/a[div]/@href")
        au_elements = administrative_unit_selector.extract()

        for el in au_elements:
            # yield {
            #     "element": el
            # }
            yield response.follow(el, callback=self.parse_administrative_unit_add_settlement)

    def parse_administrative_unit_add_settlement(self, response: TextResponse):
        # don't select lists without items (without a div that has an item count)
        settlement_selector = response.xpath("//ul[@id = 'facetNaselja']/div/li/a[div]/@href")
        settlement_elements = settlement_selector.extract()

        # we assume there are some, otherwise our assumption is incorrect
        if not settlement_elements:
            raise AssertionError("No settlements to choose from, assumption incorrect.")

        for el in settlement_elements:
            # yield {
            #     "element": el
            # }
            yield response.follow(el, callback=self.parse_list_of_properties)

    def parse_list_of_properties(self, response: TextResponse):
        # global parameters from the sidebar
        sidebar_selector = response.xpath("//div[@id = 'vsebina200']")

        listing_type = sidebar_selector.xpath("(div/div[@class = 'params']//strong)[2]/text()").extract_first()
        property_type = sidebar_selector.xpath("(div/div[@class = 'params']//strong)[3]/text()").extract_first()
        region = sidebar_selector.xpath("(div/div[@class = 'params']//strong)[4]/text()").extract_first()

        administrative_unit = sidebar_selector.xpath(
            ".//ul[@id = 'facetUE']/li/a[@class = 'act']/strong/text()").extract_first()
        settlement = sidebar_selector.xpath(
            ".//ul[@id = 'facetNaselja']/li/a[@class = 'act']/strong/text()").extract_first()

        listing_selector = response.xpath("//div[@itemtype = 'http://schema.org/ListItem']")
        for listing in listing_selector:
            property_attributes = listing.xpath(".//div[@class = 'atributi']").extract_first()
            property_attributes_text = BeautifulSoup(property_attributes, features="lxml").get_text().strip().strip(",")
            floor_at = None
            floor_max = None
            year = None
            complete_property_size_from_attributes = None
            # to message when there is something unrecognised
            for at in property_attributes_text.split(", "):
                at = at.strip()
                if at == "":
                    continue
                atlower = at.lower()
                if atlower.startswith("nadstropje: "):
                    floor = at[len("nadstropje: "):]
                    floorsplit = floor.split("/")
                    if len(floorsplit) == 1:
                        floor_at = floorsplit[0]
                        floor_max = None
                    elif len(floorsplit) == 2:
                        floor_at = floorsplit[0]
                        floor_max = floorsplit[1]
                    else:
                        raise AssertionError("Unknown floorsplit: {}".format(floor))

                elif atlower.startswith("leto: "):
                    year = int(at[len("leto: "):])
                elif atlower.startswith("zemljišče: "):
                    complete_property_size_from_attributes = at[len("zemljišče: "):]
                else:
                    # this is sometimes impossible to fix due to impurities in the source
                    self.logger.warning("Unrecognised attribute: '{}'".format(at))

            description = listing.xpath(".//div[@class = 'kratek']/text()").extract_first()
            size_from_description = None
            room_count = None
            for i, desc_part in enumerate(description.split(", ")):
                desc_part = desc_part.strip()
                # this is "official" in the first few parts, then it's the human written description
                if i <= 2 and desc_part.endswith(" m2"):
                    # if that doesn't filter out things, we give up
                    try:
                        size_from_description = self._float_string_with_thousands_separators_to_float(desc_part[:-3])
                    except ValueError:
                        pass
                if i <= 2 and ("sobno" in desc_part or
                               desc_part.endswith("garsonjera") or
                               desc_part.endswith("apartma") or
                               desc_part == "soba"):
                    room_count = desc_part

            pricing_info = listing.xpath(".//div[@class = 'main-data']")
            size_from_pricing = pricing_info.xpath("span[@class = 'velikost']/text()").extract_first()
            if size_from_pricing is not None:
                assert size_from_pricing.endswith(" m2")
                size_from_pricing = self._float_string_with_thousands_separators_to_float(size_from_pricing[:-3])
            pricing_info_from_pricing = pricing_info.xpath("span[@class = 'cena']/text()").extract_first()
            price_amount_raw = float(pricing_info.xpath("meta[@itemprop = 'price']/@content").extract_first())
            price_unit = pricing_info.xpath("meta[@itemprop = 'priceCurrency']/@content").extract_first()
            agency = pricing_info.xpath("span[@class = 'agencija']/text()").extract_first()

            # process final pricing info
            price_amount = None
            if pricing_info_from_pricing.endswith(" €/dan"):
                price_amount = price_amount_raw * 30
            elif pricing_info_from_pricing.endswith(" €/teden"):
                price_amount = price_amount_raw * 4
            elif pricing_info_from_pricing.endswith(" €") or pricing_info_from_pricing.endswith(" €/mesec"):
                price_amount = price_amount_raw
            elif pricing_info_from_pricing.endswith(" €/leto"):
                price_amount = price_amount_raw / 12
            elif pricing_info_from_pricing.endswith(" €/m2") or pricing_info_from_pricing.endswith(" €/m2/mesec"):
                price_amount = price_amount_raw * size_from_pricing
            elif pricing_info_from_pricing.endswith(" €/m2/leto"):
                price_amount = price_amount_raw * size_from_pricing / 12
            else:
                self.logger.warning("Unknown pricing format: '{}'".format(pricing_info_from_pricing))

            yield {
                "id": int(listing.xpath(".//h2[@itemprop = 'name']/a/@title").extract_first()),
                "link": listing.xpath(".//meta[@itemprop = 'mainEntityOfPage']/@content").extract_first(),
                "list_url": response.url,
                "listing_type": listing_type,
                "property_type": property_type,
                "region": region,
                "region_latitude": self._get_region_latlon(region)[0],
                "region_longitude": self._get_region_latlon(region)[1],
                "administrative_unit": administrative_unit,
                "settlement": settlement,
                "title": listing.xpath(".//span[@class = 'title']/text()").extract_first(),
                # "property_attributes": property_attributes,
                # "property_attributes_text": property_attributes_text,
                "floor_at": floor_at,
                "floor_max": floor_max,
                "year": year,
                "complete_property_size_from_attributes": complete_property_size_from_attributes,
                "description": description,
                "size_from_description": size_from_description,
                "room_count": room_count,
                # "pricing_info": pricing_info.extract_first(),
                "size_from_pricing": size_from_pricing,
                # "pricing_info_from_pricing": pricing_info_from_pricing,
                # "price_amount_raw": price_amount_raw,
                "price_amount": price_amount,
                "price_unit": price_unit,
                "agency": agency
            }

        pagination_next_url = response.xpath(
            "//div[@id = 'pagination']/ul/li[@class = 'paging_next']/a/@href").extract_first()
        yield response.follow(pagination_next_url, callback=self.parse_list_of_properties)

    @staticmethod
    def _get_region_latlon(region: str) -> Tuple[float, float]:
        return COORDINATES_STATISTICAL_AREA[region.lower()]

    @staticmethod
    def _float_string_with_thousands_separators_to_float(float_string: str) -> float:
        # first remove the thousands separator, then convert the decimal separator to a dot
        return float(float_string.replace(".", "").replace(",", "."))
