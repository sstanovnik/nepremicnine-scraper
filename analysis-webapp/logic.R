library(jsonlite)
library(maps)
library(viridis)
library(ggrepel)
library(gtools)
library(plyr)

loadNepData = function() {
    json_file = readLines(con = file("../output.json", encoding = "utf-8"))
    df = fromJSON(json_file)

    # sanity
    stopifnot(nrow(df) == length(unique(df$id)))

    df
}

generateScattermapPlot = function(from_df) {
    # columns:
    #  - latitude
    #  - longitude
    #  - value
    #  - text

    slovenia_boundaries = map_data("world") %>% filter(region == "Slovenia")

    ggplot() +
        geom_polygon(data = slovenia_boundaries, aes(x = long, y = lat), fill = "grey", alpha = 0.4) +
        geom_point(data = from_df, aes(x = longitude, y = latitude, size = value, colour = value)) +
        geom_text(data = from_df, aes(x = longitude, y = latitude, label = paste0(text, ": ", value)), size = 3.5, nudge_y = -0.08, nudge_x = 0.1) +
        theme_void() +
        scale_size_continuous(range = c(1, 20), name = "") +
        scale_color_viridis(name = "") +
        coord_map() +
        theme(legend.position = c(0.9, 0.2)) +
        guides(colour = guide_legend())
}

customOrderingRoomCount = function(input_df) {
    # negative values because things are sorted descending
    as.numeric(plyr::revalue(input_df$room_count, c(
        "soba" = -1,
        "1-sobno" = -2,
        "1,5-sobno" = -3,
        "2-sobno" = -4,
        "2,5-sobno" = -5,
        "3-sobno" = -6,
        "3,5-sobno" = -7,
        "4-sobno" = -8,
        "4,5-sobno" = -9,
        "5 in večsobno" = -10,
        "apartma" = -11,
        "poslovni apartma" = -12
    )))
}

customOrderingFloor = function(variable_name) {
    function(input_df) {
        col = c(unlist(input_df[,variable_name], use.names = FALSE))

        split_list = strsplit(col, "+", fixed = TRUE)
        split_first_elements = sapply(split_list, function(sublist) sublist[[1]])
        # only those that can be mapped are mapped, others are left as-is
        pseudo_floor_number_partials = plyr::revalue(split_first_elements, c(
            "2K" = 2,
            "K" = 1,
            "PK" = 0.5,
            "KP" = 0.5,
            "P" = 0,
            "VP" = -0.5,
            "M" = -0.6,
            "ME" = -0.6,
            "P1" = -0.8
        ))
        # invert signs here because of descending order
        -as.numeric(pseudo_floor_number_partials)
    }
}

# custom ordering function accepts a df and spits out a vector of numbers number
generateOrderedHistogram = function(from_df, variable_name, color_variable_name = NULL, custom_ordering_function = NULL) {
    total_counts = eval(parse(text = paste0("dplyr::count(from_df, ", variable_name, ")")))
    total_counts = plyr::rename(total_counts, c("n" = "ordering_column"))

    # shitty variables are taken literally, r is retarded
    counted = if (is.null(color_variable_name)) {
        # ordering column already set, but need to set n
        total_counts$n = total_counts$ordering_column
        total_counts
    } else {
        double_grouping = eval(parse(text = paste0("dplyr::count(from_df, ", variable_name, ", ", color_variable_name, ")")))
        # adds the ordering column
        merge(double_grouping, total_counts, by = variable_name)
    }

    if (!is.null(custom_ordering_function)) {
        counted$ordering_column = custom_ordering_function(counted)
    }

    table = dplyr::arrange(counted, desc(ordering_column))
    table[[variable_name]] = factor(table[[variable_name]], levels = unique(as.character(table[[variable_name]])))

    # r is retarded again
    if (is.null(color_variable_name)) {
        eval(parse(text = paste0("plot_ly(table, x = ~", variable_name, ", y = ~n, type = \"bar\")")))
    } else {
        eval(parse(text = paste0("plot_ly(table, x = ~", variable_name, ", color = ~", color_variable_name, ", y = ~n, type = \"bar\")"))) %>% layout(barmode = "stack")
    }
}
