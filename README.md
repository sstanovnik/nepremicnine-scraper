# nepremicnine.net scraper and analyser

This is a scraper (using `scrapy`) and analyser (using R and Shiny) for listings on nepremicnine.net.

Scraping that site is [against the terms of service](https://www.nepremicnine.net/pogoji-uporabe.html). I do not care. This is beneficial to everyone.

Why is this in English when the site is in Slovene and intended for a Slovene audience? Yeah. Why not.

## Notes

 * Scraping creates a ~150 MB cache directory and a ~16 MB `output.json` file.
 * Subsequent scrapes do not require an internet connection, as the cache is used if present. This is useful for changing parsing logic.
 * `output.py` is not deleted, but instead appended to, each run. This is a `scrapy` legacy design decision.
 * The scraper scrapes a subset of the site by default: only regions in Slovenia, only sell and rent offers and only houses, apartments and land lots. Filters are in `nep_spider.py`.
 * The R code has been written and used with RStudio. It _should_ work elsewhere. However, don't run with the "Run App" button. Run the script directly.
 * The scraper works as of 2018-01-10. Changes to the site layout may make the parser not work.
 * The scraper is not nice: it does not obey `robots.txt` and poses as Firefox 64. This is because the site uses the user agent to change server-side processing. However, the scrape rate is 4 Hz, which is reasonable.

## Running

Tested with Python 3.6 and R 3.4.3.

```bash
python3 -m venv .venv
source .venv/bin/activate.fish
pip install --upgrade pip
pip install -r requirements.txt

# scrapes, creates .scrapy/ and output.json
python main.py

cd analysis-webapp
# whatever R environment to run app.R 
```

## Analysis webpage screenshot

![Analysis webpage screenshot](screenshot.png)
